/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evive;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import com.google.gson.*;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Brijesh
 */


/*-----------------------------------------------------------------------------*/
/*   Notes:                                                                    */
/*     1) I am sorry to inform you that this program takes around 5 minutes    */
/*        to retrieve the actors and actress which are in at least one movie   */
/*        and at least one tvEpisodes in December 2017. And the reason behind  */
/*        this is that I made the API call in the for loop as I do not have    */
/*        any information about the API such that I can retrieve all the info  */
/*        about the movies and tvEpisodes in single API call.                  */
/*     2) Another thing I want to mention is that as the database has security */
/*        restriction for consecutive API call using one API key, we can make  */
/*        only 40 API call in a raw. After 40 API call, we need to make a halt */
/*        for some amount of time and then we can start API calling. So for    */ 
/*        that I have use Thread.Sleep(millisecond) and make a halt of 7       */
/*        seconds after 40 consecutive API call.                               */
/*-----------------------------------------------------------------------------*/


public class Evive {

    public static int tooManyRequestCounter = 0;

    public static void main(String[] args) throws MalformedURLException, IOException, InterruptedException {

        //used to store unique movieIds
        Set<Integer> movieIds = new HashSet<Integer>();

        //used to store unique movieIds
        Set<Integer> tvIds = new HashSet<Integer>();

        //used to store actor or actress of movie
        Set<String> movieActors = new HashSet<String>();

        //used to store actor or actress of tvEpisodes
        Set<String> tvActors = new HashSet<String>();

        // load movies which has primary_released_date in December 2017
        initMovieAndTvEpisodes(movieIds, "movie");

        for (int movieId : movieIds) {
            String response = retrieveMovieOrTVInfo(movieId, "movie");
            if (response != null) {
                tooManyRequestCounter++;
                if (tooManyRequestCounter == 39) {
                    Thread.sleep(7000);
                    tooManyRequestCounter = 0;
                }
                fillUpActors(response, movieActors);
            }
        }

        // load tvEpisodes which has first_air_show in December 2017
        initMovieAndTvEpisodes(tvIds, "tv");

        for (int tvId : tvIds) {
            String response = retrieveMovieOrTVInfo(tvId, "tv");
            if (response != null) {
                tooManyRequestCounter++;
                if (tooManyRequestCounter == 39) {
                    Thread.sleep(7000);
                    tooManyRequestCounter = 0;
                }
                fillUpActors(response, tvActors);
            }
        }

        Set<String> actorsInMoviesAndTvEpisodes = new HashSet<String>(movieActors);
        actorsInMoviesAndTvEpisodes.retainAll(tvActors);

        //print the actors which are in at least one movie as well as in atleast one tvEpisodes and realsed in December 2017
        for (String actor : actorsInMoviesAndTvEpisodes) {
            System.out.println(actor);
        }

    }

    //below method loads Ids of movies and TvEpisodes realsed in December 2017 
    public static void initMovieAndTvEpisodes(Set<Integer> ids, String choice) throws InterruptedException {

        try {
            String response = discover(1, choice);
            if (response != null) {
                JsonElement movieElement = new JsonParser().parse(response);
                tooManyRequestCounter++;
                JsonObject movieObject = movieElement.getAsJsonObject();
                int totalPages = movieObject.get("total_pages").getAsInt();

                fillUpIds(movieObject, ids);

                if (totalPages > 1) {

                    for (int page = 2; page <= totalPages; page++) {
                        if (tooManyRequestCounter == 40) {
                            Thread.sleep(7000);
                            tooManyRequestCounter = 0;
                        }

                        movieElement = new JsonParser().parse(discover(page, choice));
                        movieObject = movieElement.getAsJsonObject();
                        fillUpIds(movieObject, ids);
                        tooManyRequestCounter++;
                        //    System.out.println(tooManyRequestCounter);
                    }
                }
            }
        } catch (IOException e) {
            System.out.println(e);
        }

    }

    //below method generate a API for retrieving the movies and tvEpisodes released in December 2017.
    public static String discover(int pageNumber, String choice) throws MalformedURLException, IOException, InterruptedException {
        String baseUrl = "http://api.themoviedb.org/3/";
        String apiKey = "606aaffd7ca10f0b80804a1f0674e4e1";
        String release_date_gte = "2017-12-01";
        String release_date_lte = "2017-12-31";
        String url = "";
        if (choice == "tv") {
            url = baseUrl + "discover/" + choice + "?first_air_date.gte=" + release_date_gte + "&first_air_date.lte=" + release_date_lte + "&api_key=" + apiKey + "&page=" + pageNumber;
        } else {
            url = baseUrl + "discover/" + choice + "?primary_release_date.gte=" + release_date_gte + "&primary_release_date.lte=" + release_date_lte + "&api_key=" + apiKey + "&page=" + pageNumber;
        }
        return getConnection(url);
    }

    //below method generate a API for retrieving the invidual movie or tvEpisode information.
    public static String retrieveMovieOrTVInfo(int id, String choice) throws MalformedURLException, IOException {
        String baseUrl = "http://api.themoviedb.org/3/";
        String apiKey = "606aaffd7ca10f0b80804a1f0674e4e1";
        String url = "";
        if (choice == "tv") {
            url = baseUrl + choice + "/" + id + "/credits?api_key=" + apiKey;
        } else {
            url = baseUrl + choice + "/" + id + "/credits?api_key=" + apiKey;
        }
        return getConnection(url);
    }

    //below method make a connection with database and return the response.
    public static String getConnection(String url) throws IOException {
        URL DiscoverMovieUrl = new URL(url);

        HttpURLConnection con = (HttpURLConnection) DiscoverMovieUrl.openConnection();

        // set connection request type
        con.setRequestMethod("GET");

        // set connection request header
        con.setRequestProperty("Content-Type", "application/json");

        //set connection timeout
        con.setConnectTimeout(5000);

        String response = "";
        try {
            response = new BufferedReader(
                    new InputStreamReader(con.getInputStream())).readLine();

        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
        con.disconnect();

        if (response == "") {
            return null;
        } else {
            return response;
        }

    }

    //Util method
    public static void fillUpIds(JsonObject movieObject, Set<Integer> ids) {
        JsonArray jarray = movieObject.getAsJsonArray("results");
        int resultSize = jarray.size();
        int record = 0;
        if (resultSize > 0) {
            try {
                while (record < resultSize) {
                    ids.add(jarray.get(record).getAsJsonObject().get("id").getAsInt());
                    record++;
                }
            } catch (Exception e) {
                System.out.print(e);
            }
        }

    }

    //Util method
    public static void fillUpActors(String response, Set<String> actors) {
        JsonElement movieElement = new JsonParser().parse(response);
        JsonObject movieObject = movieElement.getAsJsonObject();
        JsonArray castArray = movieObject.getAsJsonArray("cast");
        int resultSize = castArray.size();
        int record = 0;
        if (resultSize > 0) {
            try {
                while (record < resultSize) {
                    actors.add(castArray.get(record).getAsJsonObject().get("name").getAsString());
                    record++;
                }
            } catch (Exception e) {
                System.out.print(e);
            }
        }

    }

}
